from django.contrib import admin
from .models import Car, Country, Part

# Register your models here.
admin.site.register(Car)
admin.site.register(Country)
admin.site.register(Part)

