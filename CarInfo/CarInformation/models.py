from django.db import models

# Create your models here.
class Country(models.Model):
    COUNTRY_NAME = [
        ('DE', 'Германия'),
        ('US', 'США'),
        ('JP', 'Япония'),
        ('RU', 'Россия'),
        ('FR', 'Франция'),
        ('KR', 'Корея')
    ]
    country = models.CharField(
        verbose_name='Страна производства',
        max_length=50,
        choices=COUNTRY_NAME,
        default='Германия'
    )
    def __str__(self):
        return self.country


class Car(models.Model):
    Type_Car = [
        ('SE', 'Седан'),
        ('CU', 'Купе'),
        ('XE', 'Хэтчбек'),
        ('UN', 'Универсал'),
        ('CR', 'Кроссовер'),
        ('OF', 'Внедорожник'),
    ]
    CarType = models.CharField(
        verbose_name="Вид автомобиля",
        max_length=15,
        choices=Type_Car,
        default='Седан',
    )
    title = models.CharField(
        verbose_name='Наименование автомобиля',
        max_length=150,
    )
    odometr = models.PositiveIntegerField(
        verbose_name='Пробег',
    )
    year = models.PositiveIntegerField(
        verbose_name='Год выпуска',
    )

    def __str__(self):
        return self.title

class Part(models.Model):
    id_car = models.ForeignKey(
        Car,
        verbose_name='Автомобиль',
        on_delete=models.CASCADE,
    )
    id_country = models.ForeignKey(
        Country,
        verbose_name='Страна производства',
        on_delete=models.CASCADE,
    )
    name = models.CharField(
        verbose_name='Наименование',
        max_length=200,
    )

    def __str__(self):
        return self.name
