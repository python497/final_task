from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import viewsets
from .models import Car, Country, Part
from .serializers import CarSerializer, CountrySerializer, PartSerializer

# Create your views here.
class CarViewSet(viewsets.ModelViewSet):
    serializer_class = CarSerializer
    queryset = Car.objects.all()

class CountryViewSet(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer

class PartViewSet(viewsets.ModelViewSet):
    queryset = Part.objects.all()
    serializer_class = PartSerializer
