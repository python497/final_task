from django.urls import path, include
from .views import CarViewSet, PartViewSet, CountryViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'car', CarViewSet, basename='car')
router.register(r'part', PartViewSet, basename='part')
router.register(r'country', CountryViewSet, basename='country')

urlpatterns = router.urls
